# Fastvue Syslog Server

This project provides a Custom Python Script Sensor for PRTG that allows it to retrieve statistics from the API of a  Fastvue Syslog Server. The project also includes a “helper script” that exports source device details, making it easier to setup a sensor on PRTG. 

## Installation

Simply copy the FastvueMessageStats.py file to “Python” subfolder within the PRTG “Custom Sensors” folder (e.g. - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\python).
The FastvueSourceList.py helper file can be run from any PC with Python3 installed. It does not to run from the PRTG server.


## Usage

Assign a new Python Script Advanced Sensor to the device that is generating the Syslog messages, NOT the Fastvue server.

The sensor requires four parameters to passed out to the script:

- Device ID - a unique hex identifier for the log source (see below)
- Server – the IP address of the Fastvue server and the port number (default 47279).
- User & Password – credentials for the Fastvue server


The parameters need to be formatted as a Python “dictionary”, which can be laborious to type out, so to simplify things, the “helper script” can be used to query the Fastvue server and export the details of all defined log sources to a text file, along with a pre-formatted parameter string that can be copied and pasted into the sensor config. The script can be run from any PC that has Python 3 installed and doesn’t need to be run from the PRTG server.

The resulting output file lists the Device ID, Hostname, IP Address and the complete PRTG Parameter String, including credentials. This can then simply be copied and pasted into the sensor config page.

If you don’t want to use the helper script to construct the parameters dictionary, you can enter the details manually:

{"deviceID": "2902cbad6da44459ad05abd1305eed14", "server": "http://192.168.6.205:47279", "user": "admin", "password": "password"}

The deviceID is displayed in the URL of the Fastvue server, when querying the specific log source.

The sensor returns 13 channels of information about the log source
- Total Message Count
- No of Messages Received Today
- Size of Messages Received Today
- No of Messages Received Over Last Four days
- Size of Messages Received Over Last Four Days


If desired thresholds can be assigned to the channels, in the usual way.

The freeware Fastvue Syslog Server is available here - https://www.fastvue.co/syslog/ 

More information about integrating PRTG with Fastvue Syslog Server can be found in this blog article:
https://blog.paessler.com/how-to-get-the-most-out-of-syslog-monitoring-with-prtg-and-fastvue

