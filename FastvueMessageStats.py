# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name             SyslogMessageStats.py
# Description      PRTG Python Script Advanced Sensor to retrieve per device Syslog message count & file size  
#                  stats from a Fastvue Syslog server API.
#-------------------
# Notes
#
# - Modules "requests", "sys" and "JSON-python-module" must be installed and available to the PRTG Python instance.
# 
# - The sensor manual page can be found here - https://www.paessler.com/manuals/prtg/python_script_advanced_sensor
#
# - This A blog post explains how to setup and use the sensor -  
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      27/07/2021  Initial Release
#
# ------------------
# Paessler AG
# ------------------

import json
import requests
import sys

from prtg.sensor.result import CustomSensorResult
from prtg.sensor.units import ValueUnit

if __name__ == "__main__":
    # Initializing some variables to make sure they are present when called
    csr = CustomSensorResult(text="")

    try:
        # Loading data dictionary passed from PRTG
        prtgParam = json.loads(sys.argv[1])
        params = json.loads(prtgParam["params"])

        # Authenticate and obtain a cookie
        authQuery = params["server"] + "/api/auth/login" + "?u=" + params["user"] + "&p=" + params["password"]
        authPayload = {}
        authHeader = {}
 
        authResponse = requests.request("GET", authQuery, headers=authHeader, data=authPayload)
        tCookie = authResponse.text.strip('\"')

        # Prepare & run API query

        apiQuery = params["server"] + "/api/sources/stats?id=" + params["deviceID"]

        apiPayload={}
        apiHeaders = {'Cookie': 't=' + tCookie}

        apiResponse = requests.request("GET", apiQuery, headers=apiHeaders, data=apiPayload)

        apiJsonResponse = json.loads(apiResponse.text)

        ###############################################################
        # Messages Stats

        # Get total message count for device
        apiTotMessages = apiJsonResponse["messages"]

        # Get msg count & size for today
        size = (len(apiJsonResponse["dates"]) -1)
        apiMsgToday = apiJsonResponse["dates"][size]["messages"]
        apiSizeToday = apiJsonResponse["dates"][size]["size"]

        # Create PRTG channels for total message & today's stats
        csr.add_primary_channel(name  ="01. Total Message Count",
            value = apiTotMessages,
            is_float = False)

        csr.add_channel(name = "02. Msg Count Today",
            value = apiMsgToday,
            is_float = False)

        csr.add_channel(name = "03. Msg Size Today",
            value = (apiSizeToday //1024),
            is_float = False,
            unit = "KiloByte")
        
        # Loop through previous 5 days to get msg count & size
        for day in range(2,7):
    
            apiMessages = (len(apiJsonResponse["dates"]) - day)
            apiMsgCount = apiJsonResponse["dates"][apiMessages]["messages"]
            apiMsgSize = apiJsonResponse["dates"][apiMessages]["size"]

            # Create channels for previous 5 day's stats
            csr.add_channel(name = "Msg Count " + str(day -1) + " Day(s) Prior",
                value = apiMsgCount,
                is_float = False)

            csr.add_channel(name = "Msg Size " + str(day -1) + " Day(s) Prior",
                value = (apiMsgSize //1024),
                is_float = False,
                unit = "KiloByte")

                
    except KeyError as k:
        csr.text = "Python Script argument error"
        csr.error = f"Python Script argument error: {str(k)}"
    except Exception as e:
        csr.text = "Python Script execution error"
        csr.error = f"Python Script execution error: {str(e)}"

    finally:
        # To reduce overhead do the output in finally
        sys.stdout.write(csr.json_result)