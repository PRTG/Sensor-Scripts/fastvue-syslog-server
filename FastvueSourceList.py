# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name             SyslogSourceList.py
# Description      Helper script to list all Fastvue Syslog sources and save deviceID, hostname, IP and PRTG param string to a file.
#                  The param string can be used to configure PRTG sensors that return Syslog source stats.
#-------------------
# Notes
#
# - Modules "requests" and "JSON-python-module" and "getpass" must be installed and available.
# - This script can run from any system with Python 3 installed, that can connect to the Fastvue server. It does not need
# - to be run on the PRTG server.
# 
# - The output file will overwrite any existing file of the same name - choose your output filename wisely!
# 
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      27/07/2021  Initial Release
#
# ------------------
# Paessler AG
# ------------------


import json
import requests
import getpass


# Prompt for Fastvue server details
print("Enter Fastvue server URL and port - e.g. http://192.168.1.100:47279 ")
server = input("Fastvue server: ")

# Prompt for Fastvue server credentals
print("Enter Fastvue server username: ")
username = input("Fastvue username: ")
print("Enter Fastvue password: ")
password = getpass.getpass("Fastvue Password: ")

# Prompt for destination file
print("Enter destination folder & filename for output report - e.g. c:/temp/logsources.txt (Will overwrite an existing file!)" )
fileDest = input("Destination folder & file: ")
f = open(fileDest, "w+")

# Authenticate and obtain token
authQuery = server + "/api/auth/login" + "?u=" + username + "&p=" + password
authPayload = {}
authHeader = {}
 
authResponse = requests.request("GET", authQuery, headers=authHeader, data=authPayload)
tCookie = authResponse.text.strip('\"')

# Make API call for Syslog sources
apiQuery = server + "/api/sources/list"
payload={}
headers = {'Cookie': 't=' + tCookie}

response = requests.request("GET", apiQuery, headers=headers, data=payload)
json_response = json.loads(response.text)

# Write output file headings
f.write("Device ID" + "                               " + "Host Name" + "               " + "IP Address" + "      " + "PRTG Param String\r")
f.write("================================" + "        " + "==============" + "          " + "==========" + "      " + "=================\r\n")

# Loop through defined syslog sources and save details to the file
for source in range(len(json_response)):
    f.write(json_response[source]["id"] + '\t' + json_response[source]["sourceHost"] + '\t' '\t' + json_response[source]["sourceIP"] + '\t' + '{"deviceID": ' + '"' + (json_response[source]["id"]) +'"' + ', ' + '"server": ' + '"' + server +'"' + ', ' + '"user": ' + '"' + username +'"' + ', ' + '"password": ' + '"' + password +'"' + '}' "\r")
    
# Close file and quit
f.close
print("")
print("Done.")

